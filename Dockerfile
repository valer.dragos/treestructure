FROM python:3.6

RUN apt-get update

# install dependencies
ADD . /code
RUN pip install -r /code/requirements.txt

# declare various docker params
VOLUME /code
EXPOSE 8000

CMD /bin/bash /code/start.sh