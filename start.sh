#!/bin/bash

# give PG enough time to start
sleep 5

python /code/manage.py makemigrations
python /code/manage.py migrate
python /code/manage.py loaddata dbdump sqlitetreestructuredump

# important to bind to 0.0.0.0 so it will be accessible from outside
# the container
python /code/manage.py runserver 0.0.0.0:8000