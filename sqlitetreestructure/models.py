# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid

from django.db import models

# Create your models here.


class Node(models.Model):
    id = models.IntegerField(primary_key=True, editable=False, auto_created=True)
    parent_id = models.IntegerField(default=0)
    root_id = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    pos = models.IntegerField(default=0)

    def __str__(self):
        return "Id:{0}, ParentId:{1}, Height:{2}, Position:{3}, RootId:{4}".format(self.id, self.parent_id,
                self.height, self.pos, self.root_id)