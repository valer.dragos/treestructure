# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ObjectDoesNotExist
from django.db import connection, transaction
from django.http import HttpResponse
from django.shortcuts import render
from sqlitetreestructure.models import Node


# Create your views here.

def index(request):
    return render(request, 'sqlitetreestructure/index.html')


@transaction.atomic
def overview(request):
    try:
        id_text = request.POST['id']
        id = int(id_text)
    except (KeyError, ValueError):
        # Show the insert page.
        return render(request, 'sqlitetreestructure/overview.html',
                      {'nodes': Node.objects.order_by('pos'), 'message': 'All nodes displayed!'})
    else:
        try:
            parentNode = Node.objects.get(id=id)
        except ObjectDoesNotExist:
            return render(request, 'sqlitetreestructure/overview.html',
                          {'message': "Parent with id:{0} does not exist!".format(id)})
        else:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT pos FROM sqlitetreestructure_node where height <= {0} AND pos > {1} ORDER BY pos LIMIT 1".format(
                        parentNode.height, parentNode.pos))
                next_node_pos = cursor.fetchone()
                if next_node_pos:
                    query = Node.objects.raw(
                        'SELECT id, parent_id, root_id, height FROM sqlitetreestructure_node where height > %s AND pos BETWEEN %s and %s ORDER BY pos'
                        , [parentNode.height, parentNode.pos, next_node_pos[0]])
                else:
                    query = Node.objects.raw(
                        'SELECT id, parent_id, root_id, height FROM sqlitetreestructure_node where height > %s AND pos > %s ORDER BY pos'
                        , [parentNode.height, parentNode.pos])
                return render(request, 'sqlitetreestructure/overview.html',
                              {'nodes': query, 'message': 'All children of node with id:{0}'.format(id)})


@transaction.atomic
def insert(request):
    try:
        parent_id_text = request.POST['parent_id']
        parent_id = int(parent_id_text);
    except (KeyError, ValueError):
        # Show the insert page.
        return render(request, 'sqlitetreestructure/insert.html',
                      {'message': 'Insert a numeric parent id to add a new node.',
                       'nodes': Node.objects.order_by('pos')})
    else:
        # Parent id specified
        if parent_id:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT id, root_id, height, pos FROM sqlitetreestructure_node WHERE id = {0} LIMIT 1".format(
                        parent_id))
                parent_query = cursor.fetchone()
                # Parent id found
                if parent_query:
                    cursor.execute(
                        "UPDATE sqlitetreestructure_node SET pos = pos+1 WHERE pos > {0}".format(parent_query[3]))
                    new_node = Node(parent_id=parent_query[0], root_id=parent_query[1], height=parent_query[2] + 1,
                                    pos=parent_query[3] + 1)
                    new_node.save()
                    return render(request, 'sqlitetreestructure/insert.html',
                                  {'node': new_node, 'nodes': Node.objects.order_by('pos')})
                # Parent does not exist
                else:
                    return render(request, 'sqlitetreestructure/insert.html', {
                        'error_message': 'Could not insert new node, parent with id:{0} does not exist!'.format(
                            parent_id), 'nodes': Node.objects.order_by('pos')})
        # Add to root or create root if db empty.
        else:
            with connection.cursor() as cursor:
                cursor.execute('SELECT MAX(pos) FROM sqlitetreestructure_node')
                highest_pos = cursor.fetchone()
                if highest_pos[0] is not None:  # There are items in the db.
                    root = Node.objects.raw(
                        'SELECT id, root_id  FROM sqlitetreestructure_node WHERE height = 0 LIMIT 1')[0]
                    new_node = Node(parent_id=root.id, root_id=root.root_id, height=1, pos=highest_pos[0] + 1)
                    new_node.save()
                    return render(request, 'sqlitetreestructure/insert.html',
                                  {'node': new_node, 'nodes': Node.objects.order_by('pos')})
                else:  # Create the node
                    new_node = Node(id=0, parent_id=-1, root_id=0, height=0, pos=0)
                    new_node.save()
                    return render(request, 'sqlitetreestructure/insert.html',
                                  {'node': new_node, 'nodes': Node.objects.order_by('pos')})


@transaction.atomic
def move(request):
    try:
        child_id_text = request.POST['child_id']
        new_parent_id_text = request.POST['new_parent_id']
        if child_id_text == new_parent_id_text:
            return render(request, 'sqlitetreestructure/move.html',
                          {'error_message': 'Ids match.', 'nodes': Node.objects.order_by('pos')})
    except KeyError:
        return render(request, 'sqlitetreestructure/move.html', {'nodes': Node.objects.order_by('pos')})
    else:
        try:
            child_id_nr = int(child_id_text)
            new_parent_id_nr = int(new_parent_id_text)
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT id, parent_id, root_id, height, pos FROM sqlitetreestructure_node where id == {0} OR id == {1} LIMIT 2".format(
                        child_id_nr, new_parent_id_nr))
                nodes = cursor.fetchall()
                child = Node(id=nodes[0][0], parent_id=nodes[0][1], root_id=nodes[0][2], height=nodes[0][3],
                             pos=nodes[0][4])
                new_parent = Node(id=nodes[1][0], parent_id=nodes[1][1], root_id=nodes[1][2], height=nodes[1][3],
                                  pos=nodes[1][4])
                if child.id == new_parent_id_nr:
                    aux = child
                    child = new_parent
                    new_parent = aux

        except (ValueError, IndexError):
            return render(request, 'sqlitetreestructure/move.html',
                          {'error_message': 'Please insert valid ids.', 'nodes': Node.objects.order_by('pos')})
        else:
            if child.parent_id == new_parent.id:
                return render(request, 'sqlitetreestructure/move.html',
                              {'error_message': 'Child already inside new parent.',
                               'nodes': Node.objects.order_by('pos')})
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT pos FROM sqlitetreestructure_node where height <= {0} AND pos > {1} ORDER BY pos LIMIT 1".format(
                        child.height, child.pos))
                next_node_pos_query = cursor.fetchone()
                if new_parent.pos > child.pos and (not next_node_pos_query or new_parent.pos < next_node_pos_query[
                    0]):  # new parent is child of child
                    return render(request, 'sqlitetreestructure/move.html',
                                  {'error_message': 'New parent cannot be a child of the child.',
                                   'nodes': Node.objects.order_by('pos')})
                else:
                    cursor.execute('SELECT MAX(pos) FROM sqlitetreestructure_node')
                    max_pos = cursor.fetchone()[0]
                    if next_node_pos_query:
                        next_node_pos = next_node_pos_query[0]
                    else:
                        next_node_pos = max_pos + 1
                    cursor.execute(
                        "UPDATE sqlitetreestructure_node SET parent_id = {0} WHERE id = {1}".
                            format(new_parent.id, child.id))
                    cursor.execute(
                        "UPDATE sqlitetreestructure_node SET pos = pos+{0} WHERE pos >= {1} AND pos < {2}".
                            format(max_pos - child.pos + 1, child.pos, next_node_pos))

                    if new_parent.pos > child.pos:
                        cursor.execute(
                            "UPDATE sqlitetreestructure_node SET pos = pos+{0} WHERE pos <= {1} AND pos >= {2}".
                                format(child.pos - next_node_pos, new_parent.pos, next_node_pos))
                        cursor.execute(
                            "UPDATE sqlitetreestructure_node SET pos = pos+{0}, height = height+{1} WHERE pos > {2}".
                                format(child.pos + new_parent.pos - next_node_pos - max_pos, new_parent.height - child.height + 1, max_pos))
                    else:
                        cursor.execute(
                            "UPDATE sqlitetreestructure_node SET pos = pos+{0} WHERE pos < {1} AND pos > {2}".
                                format(next_node_pos - child.pos, child.pos, new_parent.pos))
                        cursor.execute(
                            "UPDATE sqlitetreestructure_node SET pos = pos+{0}, height = height+{1} WHERE pos > {2}".
                                format(new_parent.pos - max_pos, new_parent.height - child.height + 1, max_pos))
                    return render(request, 'sqlitetreestructure/move.html', {'message': 'Moved successfully.',
                                                                                 'nodes': Node.objects.order_by('pos')})

@transaction.atomic
def api_get_nodes(request):
    try:
        id_text = request.GET['id']
        id = int(id_text)
    except KeyError:
        query = Node.objects.raw('SELECT id, parent_id, root_id, height FROM sqlitetreestructure_node')
        return render(request, 'sqlitetreestructure/api_get_nodes.html', {'nodes': query})
    except ValueError:
        return HttpResponse("Error: Invalid 'id' parameter value. Must be an integer.")
    else:
        try:
            parentNode = Node.objects.get(id=id)
        except ObjectDoesNotExist:
            return HttpResponse("Error: There is no node with id:{0} in the database.".format(id_text))
        else:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT pos FROM sqlitetreestructure_node where height <= {0} AND pos > {1} ORDER BY pos LIMIT 1".format(
                        parentNode.height, parentNode.pos))
                next_node_pos = cursor.fetchone()
                if next_node_pos:
                    query = Node.objects.raw(
                        'SELECT id, parent_id, root_id, height FROM sqlitetreestructure_node where height > %s AND pos BETWEEN %s and %s ORDER BY pos'
                        , [parentNode.height, parentNode.pos, next_node_pos[0]])
                else:
                    query = Node.objects.raw(
                        'SELECT id, parent_id, root_id, height FROM sqlitetreestructure_node where height > %s AND pos > %s ORDER BY pos'
                        , [parentNode.height, parentNode.pos])
                return render(request, 'sqlitetreestructure/api_get_nodes.html', {'nodes': query})


@transaction.atomic
def api_reparent(request):
    try:
        child_id_text = request.GET['child_id']
        new_parent_id_text = request.GET['new_parent_id']
        if child_id_text == new_parent_id_text:
            return HttpResponse("Error: Ids match.")
        child_id_nr = int(child_id_text)
        new_parent_id_nr = int(new_parent_id_text)
    except (KeyError, ValueError):
        return HttpResponse("Error: Invalid 'child_id' or 'new_parent_id' parameter values. Both must be integers.")
    else:
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT id, parent_id, root_id, height, pos FROM sqlitetreestructure_node where id == {0} OR id == {1} LIMIT 2".format(
                        child_id_nr, new_parent_id_nr))
                nodes = cursor.fetchall()
                child = Node(id=nodes[0][0], parent_id=nodes[0][1], root_id=nodes[0][2], height=nodes[0][3],
                             pos=nodes[0][4])
                new_parent = Node(id=nodes[1][0], parent_id=nodes[1][1], root_id=nodes[1][2], height=nodes[1][3],
                                  pos=nodes[1][4])
                if child.id == new_parent_id_nr:
                    aux = child
                    child = new_parent
                    new_parent = aux

        except IndexError:
            return HttpResponse("Error: Could not find new parent or child in the database.")
        else:
            if child.parent_id == new_parent.id:
                return HttpResponse("Error: Child already inside new parent.")
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT pos FROM sqlitetreestructure_node where height <= {0} AND pos > {1} ORDER BY pos LIMIT 1".format(
                        child.height, child.pos))
                next_node_pos_query = cursor.fetchone()
                if new_parent.pos > child.pos and (not next_node_pos_query or new_parent.pos < next_node_pos_query[0]):
                    return HttpResponse("Error: Cannot move. New parent is a child of child")
                else:
                    cursor.execute('SELECT MAX(pos) FROM sqlitetreestructure_node')
                    max_pos = cursor.fetchone()[0]
                    if next_node_pos_query:
                        next_node_pos = next_node_pos_query[0]
                    else:
                        next_node_pos = max_pos + 1
                    cursor.execute(
                        "UPDATE sqlitetreestructure_node SET parent_id = {0} WHERE id = {1}".
                            format(new_parent.id, child.id))
                    cursor.execute(
                        "UPDATE sqlitetreestructure_node SET pos = pos+{0} WHERE pos >= {1} AND pos < {2}".
                            format(max_pos - child.pos + 1, child.pos, next_node_pos))

                    if new_parent.pos > child.pos:
                        cursor.execute(
                            "UPDATE sqlitetreestructure_node SET pos = pos+{0} WHERE pos <= {1} AND pos >= {2}".
                                format(child.pos - next_node_pos, new_parent.pos, next_node_pos))
                        cursor.execute(
                            "UPDATE sqlitetreestructure_node SET pos = pos+{0}, height = height+{1} WHERE pos > {2}".
                                format(child.pos + new_parent.pos - next_node_pos - max_pos, new_parent.height - child.height + 1, max_pos))
                    else:
                        cursor.execute(
                            "UPDATE sqlitetreestructure_node SET pos = pos+{0} WHERE pos < {1} AND pos > {2}".
                                format(next_node_pos - child.pos, child.pos, new_parent.pos))
                        cursor.execute(
                            "UPDATE sqlitetreestructure_node SET pos = pos+{0}, height = height+{1} WHERE pos > {2}".
                                format(new_parent.pos - max_pos, new_parent.height - child.height + 1, max_pos))
                    return HttpResponse("Node with id:{0} was successfully parented into the node with id:{1}.".format(child_id_text, new_parent_id_text))