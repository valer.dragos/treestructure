from django.conf.urls import url

from . import views

app_name = 'sqlitetreestructure'
urlpatterns = [
    # ex: /sqlitetreestructure/
    url(r'^$', views.index, name='index'),
    # ex: /sqlitetreestructure/overview/
    url(r'^overview/$', views.overview, name='overview'),
    # ex: /sqlitetreestructure/insert/
    url(r'^insert/$', views.insert, name='insert'),
    # ex: /sqlitetreestructure/move/
    url(r'^move/$', views.move, name='move'),

    # ex: /sqlitetreestructure/api/get_nodes/
    url(r'^api/get_nodes/$', views.api_get_nodes, name='get_nodes'),
    # ex: /sqlitetreestructure/api/reparent/
    url(r'^api/reparent/$', views.api_reparent, name='reparent'),
]